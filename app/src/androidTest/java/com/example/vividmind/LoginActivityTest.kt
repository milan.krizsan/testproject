package com.example.vividmind

import android.util.Log
import androidx.test.espresso.IdlingRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.jakewharton.espresso.OkHttp3IdlingResource
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.concurrent.TimeUnit


@Suppress("DEPRECATION")
@RunWith(AndroidJUnit4::class)
class LoginActivityTest {

    @get:Rule
    val activityRule = ActivityTestRule(LoginActivity::class.java, true, false)

    private lateinit var mockWebServer: MockWebServer
    private val client = OkHttpClient.Builder()
        .connectTimeout(1, TimeUnit.SECONDS)
        .readTimeout(1, TimeUnit.SECONDS)
        .writeTimeout(1, TimeUnit.SECONDS)
        .build()

    @Before
    fun setup() {
        Log.d("LoginActivityTest", "setup")

        mockWebServer = MockWebServer()
        mockWebServer.start(8080)
        IdlingRegistry.getInstance().register(
            OkHttp3IdlingResource.create(
                "okhttp",
                client
            )
        )
    }

    @After
    fun tearDown() {
        Log.d("LoginActivityTest", "tearDown")

        mockWebServer.shutdown()
    }


    @Test
    fun testSuccessfulResponse() {
        Log.d("LoginActivityTest", "testSuccessfulResponse")

        mockWebServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                Log.d("LoginActivityTest", "1")

                var response = MockResponse().setResponseCode(200).setBody(FileReader.readStringFromFile("success_response.json"))
                return response
            }

        }
    }

    @Test
    fun testFailedResponse() {
        Log.d("LoginActivityTest", "testFailedResponse")

        mockWebServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                Log.d("LoginActivityTest", "testFailedResponse - dispatch")

                return MockResponse().throttleBody(1024, 5, TimeUnit.SECONDS)
            }
        }
    }
}