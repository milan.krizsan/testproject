package com.example.vividmind

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity() {

    private lateinit var viewModel: LoginActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        viewModel = LoginActivityViewModel()

        setButtonEnabled(false)

        setTextWatcher(edit_text_user_name, edit_text_password)
        setTextWatcher(edit_text_password, edit_text_user_name)

        button_next.setOnClickListener {
            authenticateUser()
        }

    }

    private fun authenticateUser() {
        viewModel.authenticate(edit_text_user_name.text.toString(), edit_text_password.text.toString()).observe(this, Observer {
            if (it != null) {
                if (it.responseCode == 200) {
                    val intent = Intent(applicationContext, MainActivity::class.java)
                    intent.putExtra(MainActivity.EXTRA_DATA, it)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                } else {
                    if (it.responseCode == 401) {
                        openAlert(R.string.alert_text_invalid_input)
                    } else {
                        openAlert(R.string.alert_text_unexpected)
                    }
                }
            } else {
                openAlert(R.string.alert_text_unexpected)
            }
        })
    }

    private fun openAlert(alertTextInvalidInput: Int) {
        AlertDialog.Builder(this)
            .setMessage(getString(alertTextInvalidInput))
            .setNegativeButton(R.string.alert_button, null)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .show()
    }

    private fun setTextWatcher(editText: EditText, checkedEditText: EditText) {
        editText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (checkedEditText.text.isNullOrEmpty()) {
                    setButtonEnabled(false)
                } else {
                    if (text!!.isNotEmpty()) {
                        setButtonEnabled(true)
                    } else {
                        setButtonEnabled(false)
                    }
                }
            }

            override fun afterTextChanged(p0: Editable?) {
            }

        })
    }

    private fun setButtonEnabled(isEnabled: Boolean) {
        if (isEnabled) {
            button_next.alpha = 1.0f
        } else {
            button_next.alpha = 0.5f
        }
        button_next.isEnabled = isEnabled
    }
}