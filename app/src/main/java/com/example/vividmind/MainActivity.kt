package com.example.vividmind

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.vividmind.model.AuthenticateResponse

class MainActivity : AppCompatActivity() {

    companion object{
        val EXTRA_DATA = "EXTRA_DATA"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (intent != null && intent!!.hasExtra(EXTRA_DATA)) {
            val data = intent!!.getSerializableExtra(EXTRA_DATA) as AuthenticateResponse

        }
    }
}