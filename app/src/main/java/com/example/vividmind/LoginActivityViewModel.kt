package com.example.vividmind

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.vividmind.model.AuthenticateResponse
import com.example.vividmind.network.authentication.AuthenticationController

class LoginActivityViewModel : ViewModel() {

    fun authenticate(username: String, password: String): MutableLiveData<AuthenticationController.AuthenticateResult> {
        return MutableLiveData<AuthenticationController.AuthenticateResult>().also {
            AuthenticationController().authenticate(it, username, password)
        }
    }

}