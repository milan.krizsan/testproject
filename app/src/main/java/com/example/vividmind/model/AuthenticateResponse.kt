package com.example.vividmind.model

import java.io.Serializable

class AuthenticateResponse : Serializable {

    lateinit var access_token: String
    lateinit var token_type :String
    var expires_in: Int? = null
    lateinit var refresh_token: String
}