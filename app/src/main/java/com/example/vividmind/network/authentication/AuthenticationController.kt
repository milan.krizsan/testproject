package com.example.vividmind.network.authentication

import androidx.lifecycle.MutableLiveData
import com.example.vividmind.Constants
import com.example.vividmind.model.AuthenticateResponse
import com.example.vividmind.network.BaseController
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.Serializable

class AuthenticationController : BaseController() {

    fun authenticate(data: MutableLiveData<AuthenticateResult>, userName: String, password: String) {
        val retrofit = createRetrofit()
        retrofit.create(AuthenticationAPI::class.java)
            .authenticate(userName, password, "password", Constants.CLIENT_ID)
            .enqueue(object : Callback<AuthenticateResult> {
                override fun onResponse(call: Call<AuthenticateResult>, response: Response<AuthenticateResult>) {
                    data.value = response.body()
                }

                override fun onFailure(call: Call<AuthenticateResult>, t: Throwable) {
                    data.value = null
                }

            })
    }

    class AuthenticateResult : Serializable {
        var response: AuthenticateResponse? = null
        var responseCode: Int = 0
    }
}