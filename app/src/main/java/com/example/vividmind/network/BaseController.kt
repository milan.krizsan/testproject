package com.example.vividmind.network

import com.example.vividmind.VividMindApp
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

open class BaseController {
    fun createRetrofit(): Retrofit {
        val httpClientBuilder = createHttpClientBuilder()
        val httpClient = httpClientBuilder.build()

        val gson = GsonBuilder()
            .setLenient()
            .create()

        val retrofit = Retrofit.Builder()
            .baseUrl(VividMindApp().getBaseUrl())
            .client(httpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))

        return retrofit.build()
    }

    private fun createHttpClientBuilder(): OkHttpClient.Builder {
        return OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .connectTimeout(2, TimeUnit.MINUTES)
            .readTimeout(2, TimeUnit.MINUTES)
            .writeTimeout(2, TimeUnit.MINUTES)
    }
}