package com.example.vividmind.network.authentication

import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface AuthenticationAPI {

    @FormUrlEncoded
    @POST("idp/api/v1/token")
    fun authenticate(
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("grant_type") grant_type: String,
        @Field("client_id") client_id: String,
    ): Call<AuthenticationController.AuthenticateResult>

    @FormUrlEncoded
    @POST("idp/api/v1/token")
    fun extendAuthentication(
        @Field("refresh_token") refresh_token: String,
        @Field("grant_type") grant_type: String,
        @Field("client_id") client_id: String
    ): Call<Void>

}
